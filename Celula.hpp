#ifndef CELULA_HPP
#define CELULA_HPP

/**
* @file Celula.hpp
* @brief Especificació de la classe <em>Celula</em>.
*/

/**
* @class Celula
* @brief Representa una cèl·lula
*/

class Celula
{

public:
    /**
    * @brief Crea un cèl·lula amb ID 0
    */
    Celula();

    /**
    * @brief Crea un cèl·lula amb un ID
    * @param ID l'ID que tindrà la cèl·lula
    * @param activa indica si la cèl·lula està activa o no
    */
    Celula(int ID, int activa);

    /**
    * @brief Ens diu si la cèl·lula està activa
    * @pre cert
    * @post retorna si la cèl·lula està activa
    * @return si la cèl·lula està activa
    */
    bool esta_activa() const;

    /**
    * @brief Retorna l'ID de la cèl·lula
    * @pre cert
    * @post retorna l'ID de la cèl·lula
    * @return l'ID de la cèl·lula
    */
    int getID() const;

private:
    /** @brief l'ID de la cèl·lula */
    int ID;
    /** @brief indica si la cèl·lula està activa */
    bool activa;
};



#endif
