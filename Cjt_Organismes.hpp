#ifndef CJT_ORGANISME_HPP
#define CJT_ORGANISME_HPP

#include <list>
#include <vector>
#include "Organisme.hpp"
#include "Ranking.hpp"
using namespace std;

/**
* @file Cjt_Organismes.hpp
* @brief Especificació de la classe <em>Cjt_Organismes</em>.
*/

/**
* @class Cjt_Organismes
* @brief Representa un conjunt d'organismes
*/

class Cjt_Organismes
{

public:

    /**
    * @brief Crea un conjunt d'organismes i els llegeix per stdin
    * @param pobl_inicial nombre d'organismes del conjunt a crear
    * @param max_poblacio nombre màxim de població històrica que pot tenir el conjunt
    */
    Cjt_Organismes(int pobl_inicial, int max_poblacio);

    /**
    * @brief Fa créixer certs organismes del conjunt
    * @pre A l'stdin hi ha un llistat dels organismes que han de créixer
    * @post Els organismes llistats han crescut
    */
    void creix_subcjt();

    /**
    * @brief Fa decréixer certs organismes del conjunt
    * @pre A l'stdin hi ha un llistat dels organismes que han de decréixer
    * @post Els organismes llistats han decrescut
    */
    void decreix_subcjt();

    /**
    * @brief Retorna la població històrica màxima que pot tenir el conjunt
    * @return la població històrica màxima
    * @pre cert
    * @post retorna la població històrica màxima
    */
    int maxim_poblacio_historica() const;

    /**
    * @brief Retorna la població històrica actual del conjunt
    * @return la població històrica actual del conjunt
    * @pre cert
    * @post retorna la població històrica actual del conjunt
    */
    int nombre_poblacio_historica() const;

    /**
    * @brief Aparella dos organismes del conjunt
    * @param ID_1 l'ID d'un dels organismes a aparellar
    * @param ID_2 l'ID de l'altre organisme a aparellar
    * @param ID_fill l'ID que tindrà el fill
    * @pre ID_1 i ID_2 estan al conjunt, són compatibles, i el conjunt no està ple
    * @post un nou organisme, resultat de la intersecció dels organismes
            ID_1 i ID_2, amb ID ID_fill s'ha afegit al conjunt
    */
    void aparella_organismes(int ID_1, int ID_2, int ID_fill);

    /**
    * @brief Aparella tots els organismes possibles
    * @return el nombre d'organismes nous
    */
    int ronda_reproduccio();

    /**
    * @brief Mostra el ranking dels aparellaments dels organismes
    * @pre cert
    * @post l'informació s'ha mostrat per stdout
    */
    void mostra_ranking() const;

    /**
    * @brief Mostra l'informació sobre l'organisme ID del conjunt, en cas que
    *        existeixi
    * @pre cert
    * @post l'informació s'ha mostrat per stdout (si l'organisme ID existeix)
    */
    void consulta_organisme(int ID);

    /**
    * @brief Ens diu el nombre d'organismes vius del conjunt
    * @return el nombre d'organismes vius del conjunt
    */
    int nombre_vius() const;

    /**
    * @brief Ens diu si tots els organismes del conjunt estan morts
    * @return si tots els organismes del conjunt estan morts
    */
    bool organismes_tots_morts() const;

    /**
    * @brief Mostra l'informació de l'última ronda d'aparellaments
    * @pre s'ha fet alguna ronda d'aparellaments prèviament
    * @post l'informació s'ha mostrat per stdout
    */
    void mostra_ultima_ronda();


private:
    /** @brief ombre d'organismes vius */
    int n_vius;
    /** @brief nombre d'organismes (vius i morts) */
    int n_pobl;
    /** @brief nombre d'organismes creats a l'última ronda de reproducció */
    int n_nous;
    /** @brief màxim d'organismes que pot tenir el conjunt */
    int max_pobl;
    /** @brief vector que conté els organismes */
    vector<Organisme> organismes;
    /** @brief matriu que conte per cada fila i, i cada columna j, si l'organisme 
    *          i s'ha aparellat amb el j en alguna ronda */
    vector<vector<bool> > m_aparellaments;
    /** @brief ranking dels organismes */
    Ranking ranking;
};



#endif
