#ifndef RANKING_HPP
#define RANKING_HPP

#include <list>
using namespace std;

/**
* @file Ranking.hpp
* @brief Especificació de la classe <em>Ranking</em>.
*/

/**
* @class Ranking
* @brief Representa un ranking
*/

class Ranking
{

public:
    /**
    * @brief Crea un ranking buit
    */
    Ranking();

    /**
    * @brief Afegeix un organisme al ranking, posant a 0 el nombre de fills
             i buida la llista de les seves parelles
    * @pre l'organisme no existeix al ranking
    * @post l'organisme ha estat afegit
    */
    void afegir_organisme(int ID);

    /**
    * @brief Posa l'aparellament {ID_parella, ID_fill} a la llista d'aparellaments
    *        de l'organisme ID
    * @pre l'organisme ID existeix, i no tenia aquest aparellament
    * @post s'ha afegit l'aparellament {ID_parella, ID_fill} a la llista d'aparellaments
    *       de l'organisme ID
    */
    void afegir_aparellament(int ID, int ID_parella, int ID_fill);

    /**
    * @brief Mostra per stdout els aparellaments del ranking
    *        ordenats de forma creixent
    * @pre cert
    * @post a stdout hi ha l'informació dels aparellaments
    */
    void mostra_aparellaments() const;

private:

    /**
    * @brief Representa l'informació d'un aparellament de l'organisme
    */
    struct Info_Aparellaments {
        int ID_parella; /**< ID de la parella */
        int ID_fill;    /**< ID del fill */
    };

    /**
    * @brief Representa l'informació dels aparellaments d'un organisme
    */
    struct Aparellaments_Organisme {
        int ID;                            /**< ID de l'organisme */
        int n_fills;                       /**< nombre de fills de l'organisme ID */
        list<Info_Aparellaments> info_ap;  /**< llista de les parelles i fills de l'organisme ID */
    };

    /** @brief conté la llista de l'informació dels aparellaments dels organismes */
    list<Aparellaments_Organisme> aparellaments;
};



#endif
