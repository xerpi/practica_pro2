TARGET = pro2.exe

SOURCES = pro2.cpp Cjt_Organismes.cpp Organisme.cpp Ranking.cpp Celula.cpp
OBJS = $(SOURCES:.cpp=.o)

CXX = g++
CXXFLAGS = -D_GLIBCXX_DEBUG -I$(INCLUDES_CPP)

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CXX) $(OBJS) -o $(TARGET)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

docu:
	doxygen Doxyfile

clean:
	rm -f $(TARGET) $(OBJS)
