#include "Ranking.hpp"
#include <iostream>

Ranking::Ranking()
{

}

void Ranking::afegir_organisme(int ID)
{
    Aparellaments_Organisme ap = {
        .ID = ID,
        .n_fills = 0
    };
    aparellaments.push_back(ap);
}


void Ranking::afegir_aparellament(int ID, int ID_parella, int ID_fill)
{
    list<Aparellaments_Organisme>::iterator it;
    it = aparellaments.begin();
    while (it->ID != ID) {
        ++it;
    }
    //it apunta al organisme ID
    it->n_fills++;
    Info_Aparellaments ap = {
        .ID_parella = ID_parella,
        .ID_fill = ID_fill
    };
    (it->info_ap).insert((it->info_ap).end(), ap);

    //ordenar el ranking
    list<Aparellaments_Organisme>::iterator it2;
    it2 = aparellaments.begin();
    bool ordenat = false;
    while (it2 != it and not ordenat) {
        if (it->n_fills > it2->n_fills) {
            aparellaments.insert(it2, *it);
            it = aparellaments.erase(it);
            ordenat = true;
        } else if (it->n_fills == it2->n_fills){
            if (it->ID < it2->ID) {
                aparellaments.insert(it2, *it);
                it = aparellaments.erase(it);
                ordenat = true;
            }
        }
        it2++;
    }
}

void Ranking::mostra_aparellaments() const
{
    list<Aparellaments_Organisme>::const_iterator it;
    it = aparellaments.begin();
    while (it != aparellaments.end()) {
        cout << it->ID << " :";
        list<Info_Aparellaments>::const_iterator it_info_ap = it->info_ap.begin();
        while (it_info_ap != it->info_ap.end()) {
            cout << " " << it_info_ap->ID_parella << " " << it_info_ap->ID_fill;
            ++it_info_ap;
        }
        cout << endl;
        ++it;
    }
}

