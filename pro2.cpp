/**
* @mainpage Disseny modular: Reproducció en el laboratori
* Pràctica de l'assignatura PRO2 que consisteix en la simulació d'organismes en un laboratori
*/

/**
* @file main.cpp
* @brief Programa principal de la pràctica <em>Reproducció en el laboratori</em>.
*/

#include <iostream>
#include "Cjt_Organismes.hpp"
#include "utils.PRO2"
using namespace std;

/**
* @enum Estat_experiment
* @brief Representa l'estat de l'experiment
*/

enum Estat_experiment {
    ACTIU,
    ORGANISMES_MORTS,
    FINALITZAT_MANUALMENT,
    LIMIT_POBLACIO
};

void llegeix_opcio(int &op)
{
    op = readint();
}

int main()
{
    int N = readint();
    int M = readint();

    Cjt_Organismes conjunt(N, M);
    Estat_experiment estat = ACTIU;

    int opcio; //Codi d'operació
    llegeix_opcio(opcio);
    if (opcio == -6) estat = FINALITZAT_MANUALMENT;
    while (estat == ACTIU) {
        switch (opcio) {
        case -1:
            conjunt.creix_subcjt();
            break;
        case -2:
            conjunt.decreix_subcjt();
            break;
        case -3: {
            cout << "RONDA DE EMPAREJAMIENTOS" << endl;
            int nous = conjunt.ronda_reproduccio();
            cout << "Nuevos organismos : " << nous << endl << endl;
            break;
        }
        case -4:
            cout << "RANKING" << endl;
            conjunt.mostra_ranking();
            cout << endl;
            break;
        case -5: {
            cout << "ORGANISMOS" << endl;
            int n = readint();
            for (int i = 0; i < n; ++i) {
                int ID = readint();
                conjunt.consulta_organisme(ID);
            }
            cout << endl;
            break;
        }
        }

        if (conjunt.organismes_tots_morts()) estat = ORGANISMES_MORTS;
        else if (conjunt.nombre_poblacio_historica() == conjunt.maxim_poblacio_historica()) {
            estat = LIMIT_POBLACIO;
        } else {
            llegeix_opcio(opcio);
            if (opcio == -6) estat = FINALITZAT_MANUALMENT;
        }
    }

    cout << "FIN" << endl << endl;
    cout << "Organismos en total : " << conjunt.nombre_poblacio_historica() << endl;
    cout << "Organismos vivos : " << conjunt.nombre_vius() << endl << endl;

    if (estat == LIMIT_POBLACIO) {
        cout << "ORGANISMOS" << endl;
        conjunt.mostra_ultima_ronda();
        cout << endl << "RANKING" << endl;
        conjunt.mostra_ranking();
    }

    return 0;
}
