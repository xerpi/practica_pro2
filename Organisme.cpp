/**
* @file Organisme.cpp
* @brief Implementació de la classe <em>Organisme</em>.
*/

#include "Organisme.hpp"


Organisme::Organisme()
{
    max_ID = 0;
    retallat = false;
    tamany = 0;
}

void Organisme::interseccio(Arbre<Celula> &t1, Arbre<Celula> &t2, int &tamany_fill)
{
    if (not t1.es_buit() and not t2.es_buit()) {
        Celula t1_arrel = t1.arrel();
        Celula t2_arrel = t2.arrel();
        Arbre<Celula> t1_fe, t1_fd;
        Arbre<Celula> t2_fe, t2_fd;

        t1.fills(t1_fe, t1_fd);
        t2.fills(t2_fe, t2_fd);

        interseccio(t1_fe, t2_fe, tamany_fill);
        interseccio(t1_fd, t2_fd, tamany_fill);

        tamany_fill += 1;

        t1.plantar(t1_arrel, t1_fe, t1_fd);
        t2.plantar(t2_arrel, t2_fe, t2_fd);
    }
}

void Organisme::i_reproduccio(Arbre<Celula> &a1, Arbre<Celula> &a2, Arbre<Celula> &fill, bool &plantar, int &max_ID_o1, int &max_ID, int &n_cel)
{
    //Cas intersecció
    if (not a1.es_buit() and not a2.es_buit()) {
        Celula c1 = a1.arrel();
        Celula c2 = a2.arrel();
        Arbre<Celula> a1_fe, a1_fd, a2_fe, a2_fd;
        a1.fills(a1_fe, a1_fd);
        a2.fills(a2_fe, a2_fd);

        Arbre<Celula> fill_e, fill_d;

        i_reproduccio(a1_fe, a2_fe, fill_e, plantar, max_ID_o1, max_ID, n_cel);
        i_reproduccio(a1_fd, a2_fd, fill_d, plantar, max_ID_o1, max_ID, n_cel);

        //Si son interseccio s'ha de plantar si o si
        Celula c_fill(c1.getID(), c1.esta_activa() or c2.esta_activa());
        fill.plantar(c_fill, fill_e, fill_d);
        if (c1.getID() > max_ID) max_ID = c1.getID();
        n_cel++;

        a1.plantar(c1, a1_fe, a1_fd);
        a2.plantar(c2, a2_fe, a2_fd);

    //Cas a1 no buit i a2 buit
    } else if (not a1.es_buit()) {
        Celula c1 = a1.arrel();
        Arbre<Celula> a1_fe, a1_fd;
        a1.fills(a1_fe, a1_fd);

        Arbre<Celula> fill_e, fill_d;
        bool plantar_e = false, plantar_d = false;
        i_reproduccio(a1_fe, a2, fill_e, plantar_e, max_ID_o1, max_ID, n_cel);
        i_reproduccio(a1_fd, a2, fill_d, plantar_d, max_ID_o1, max_ID, n_cel);

        plantar = c1.esta_activa() or plantar_e or plantar_d;
        if (plantar) {
            if (c1.getID() > max_ID) max_ID = c1.getID();
            Celula c_fill(c1.getID(), c1.esta_activa());
            fill.plantar(c_fill, fill_e, fill_d);
            n_cel++;
        }

        a1.plantar(c1, a1_fe, a1_fd);

    //Cas a2 no buit i a1 buit
    } else if (not a2.es_buit()) {
        Celula c2 = a2.arrel();
        Arbre<Celula> a2_fe, a2_fd;
        a2.fills(a2_fe, a2_fd);

        ++max_ID_o1;
        int max_ID_o1_act = max_ID_o1;

        Arbre<Celula> fill_e, fill_d;
        bool plantar_e = false, plantar_d = false;
        i_reproduccio(a1, a2_fe, fill_e, plantar_e, max_ID_o1, max_ID, n_cel);
        i_reproduccio(a1, a2_fd, fill_d, plantar_d, max_ID_o1, max_ID, n_cel);

        plantar = c2.esta_activa() or plantar_e or plantar_d;
        if (plantar) {
            if (max_ID_o1 > max_ID) max_ID = max_ID_o1;
            Celula c_fill(max_ID_o1_act, c2.esta_activa());
            fill.plantar(c_fill, fill_e, fill_d);
            n_cel++;
        } else --max_ID_o1;

        a2.plantar(c2, a2_fe, a2_fd);

    }

}

void Organisme::reproduir(Organisme &p1, Organisme &p2)
{
    bool plantar = false;
    int max_ID_o1 = p1.max_ID;
    this->max_ID = 0;
    i_reproduccio(p1.celules, p2.celules, this->celules, plantar, max_ID_o1, this->max_ID, this->tamany);
}

void Organisme::llegeix_preordre(Arbre<Celula> &t, int &tamany, int &max_ID)
{
    int n = readint();
    if (n != 0) {
        if (n > max_ID) max_ID = n;
        tamany++;

        bool activa = readbool();
        Arbre<Celula> a1, a2;
        llegeix_preordre(a1, tamany, max_ID);
        llegeix_preordre(a2, tamany, max_ID);
        t.plantar(Celula(n, activa), a1, a2);
    }
}

void Organisme::llegeix_stdin()
{
    tamany = 0;
    llegeix_preordre(celules, tamany, max_ID);
}

void Organisme::escriu_inordre(Arbre<Celula> &t)
{
    if (t.es_buit()) cout << "0 ";
    else {
        Celula c = t.arrel();
        Arbre<Celula> a1, a2;
        t.fills(a1, a2);

        escriu_inordre(a1);
        cout << c.getID() << (c.esta_activa() ? " 1" : " -1") << " ";
        escriu_inordre(a2);

        t.plantar(c, a1, a2);
    }
}


void Organisme::escriu_stdout()
{
    escriu_inordre(celules);
    cout << endl;
}

bool Organisme::es_compatible(Organisme &o)
{
    int tam1 = numero_celules();
    int tam2 = o.numero_celules();

    int tam_fill = 0;
    interseccio(celules, o.celules, tam_fill);

    return (tam_fill >= int((tam1 + tam2)/4));
}

void Organisme::creix_arbre(Arbre<Celula> &t, int &max_ID, int &tamany)
//Pre:  t = T; max_ID té l'ID màxim de t, i tamany el seu tamany
//Post: t ha crescut, i max_ID i tamany contenen
//      el ID màxim i el tamany de T, respectivament
{
    if (not t.es_buit()) {
        Arbre<Celula> a1, a2, buit;
        Celula c = t.arrel();
        t.fills(a1, a2);

        if (a1.es_buit() and a2.es_buit()) {
            a1.plantar(Celula(max_ID+1, c.esta_activa()), buit, buit);
            a2.plantar(Celula(max_ID+2, c.esta_activa()), buit, buit);

            tamany += 2;
            max_ID += 2;

        } else {
            creix_arbre(a1, max_ID, tamany);
            creix_arbre(a2, max_ID, tamany);
        }
        
        // HI: suposem que a1 i a2 ja han crescut, per tant l'arbre crescut
        //     serà el resultat de plantar l'arrel que tenia amb a1 i a2

        t.plantar(c, a1, a2);
    }
}


void Organisme::creix()
{
    if (not retallat)
        creix_arbre(celules, max_ID, tamany);
}

/**
* @brief Calcula el màxim entre dos nombres
* @pre cert
* @post retorna el màxim entre a i b
*/
#define max2(a,b) \
    ((a) > (b) ? (a) : (b))

/**
* @brief Calcula el màxim entre tres nombres
* @pre cert
* @post retorna el màxim entre a, b i c
*/
#define max3(a,b,c) \
    max2(max2(a, b), c)

void Organisme::decreix_arbre(Arbre<Celula> &t, int &max, int &tamany)
{
    Arbre<Celula> a1, a2;
    Celula c = t.arrel();
    t.fills(a1, a2);

    if (a1.es_buit() and a2.es_buit()) {
        tamany--;
    } else {
        int max1 = 0, max2 = 0;

        if (not a1.es_buit()) decreix_arbre(a1, max1, tamany);
        if (not a2.es_buit()) decreix_arbre(a2, max2, tamany);

        max = max3(c.getID(), max1, max2);

        t.plantar(c, a1, a2);
    }
}

void Organisme::decreix()
{
    retallat = true;
    int max = 0;
    decreix_arbre(celules, max, tamany);
    max_ID = max;
}


int Organisme::numero_celules()
{
    return tamany;
}


bool Organisme::esta_viu() const
{
    return (tamany != 0);
}

