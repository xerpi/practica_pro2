/**
* @file Celula.cpp
* @brief Implementació de la classe <em>Celula</em>.
*/

#include "Celula.hpp"


Celula::Celula()
{
    ID = 0;
}

Celula::Celula(int ID, int activa)
{
    this->ID = ID;
    this->activa = activa;
}

bool Celula::esta_activa() const
{
    return activa;
}

int Celula::getID() const
{
    return ID;
}

