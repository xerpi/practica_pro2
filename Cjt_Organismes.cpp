#include "Cjt_Organismes.hpp"


Cjt_Organismes::Cjt_Organismes(int pobl_inicial, int max_poblacio)
{
    n_nous = 0;
    n_vius = pobl_inicial;
    n_pobl = pobl_inicial;
    max_pobl = max_poblacio;

    m_aparellaments = vector<vector<bool> >(max_pobl, vector<bool>(max_pobl, false));
    organismes = vector<Organisme>(max_poblacio);

    for (int i = 0; i < pobl_inicial; i++) {
        organismes[i].llegeix_stdin();
        ranking.afegir_organisme(i+1);
    }

}


void Cjt_Organismes::creix_subcjt()
{
    int L = readint();
    while (L > 0) {
        int n = readint();
        if (n <= n_pobl) {
            organismes[n-1].creix();
        }
        L--;
    }
}

void Cjt_Organismes::decreix_subcjt()
{
    int L = readint();
    while (L > 0) {
        int n = readint();
        if (n <= n_pobl) {
            if (organismes[n-1].esta_viu()) {
                organismes[n-1].decreix();
                if (not organismes[n-1].esta_viu()) n_vius--;
            }
        }
        L--;
    }
}


int Cjt_Organismes::maxim_poblacio_historica() const
{
    return max_pobl;
}


int Cjt_Organismes::nombre_poblacio_historica() const
{
    return n_pobl;
}


void Cjt_Organismes::aparella_organismes(int ID_1, int ID_2, int ID_fill)
{
    organismes[ID_fill-1].reproduir(organismes[ID_1-1], organismes[ID_2-1]);
    ranking.afegir_organisme(ID_fill);
    ranking.afegir_aparellament(ID_1, ID_2, ID_fill);
    ranking.afegir_aparellament(ID_2, ID_1, ID_fill);
}


int Cjt_Organismes::ronda_reproduccio()
//Pre: cert
//Post: s'ha realitzat una ronda de reproducció, i ens diu els organismes nous
{
    vector <bool> repro_actual(n_pobl, false);
    n_nous = 0;
    int i = 0;
    
    //Inv: 0<i<n_pobl, hem reproduït si ha sigut possible els organismes
    //     [0...i], i n_nous conté el nombre d'organismes nous
    while (i < n_pobl and (n_pobl + n_nous) < max_pobl) {

        if (not repro_actual[i] and organismes[i].esta_viu()) {
            int j = i+1;
            bool trobat = false;
            
            //Inv: i<j<n_pobl, trobat diu si s'ha intentat reproduir l'organisme i amb el j
            while (j < n_pobl and not trobat and (n_pobl + n_nous) < max_pobl) {
                if (organismes[j].esta_viu() and not repro_actual[j]) {
                    if (not m_aparellaments[i][j]) {
                        bool compatibles = organismes[i].es_compatible(organismes[j]);

                        m_aparellaments[i][j] = true;
                        m_aparellaments[j][i] = true;
                        repro_actual[i] = true;
                        repro_actual[j] = true;

                        if (compatibles) {
                            aparella_organismes(i+1, j+1, n_pobl + n_nous + 1);
                            n_nous++;
                            n_vius++;
                        }
                        trobat = true;
                    }
                }
                ++j;
            }
        }
        ++i;
    }
    //Augmentem la població total
    n_pobl += n_nous;
    return n_nous;
}


void Cjt_Organismes::mostra_ranking() const
{
    ranking.mostra_aparellaments();
}

void Cjt_Organismes::consulta_organisme(int ID)
{
    if (ID <= n_pobl) {
        cout << ID << " : ";
        organismes[ID-1].escriu_stdout();
    }
}

int Cjt_Organismes::nombre_vius() const
{
    return n_vius;
}

bool Cjt_Organismes::organismes_tots_morts() const
{
    return (n_vius == 0);
}


void Cjt_Organismes::mostra_ultima_ronda()
{
    //n_nous es el nombre d'organismes que han estat creats en l'ultima ronda
    int i = n_pobl-n_nous+1;
    while (i <= n_pobl) {
        cout << i << " : ";
        organismes[i-1].escriu_stdout();
        i++;
    }
}
