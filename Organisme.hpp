#ifndef ORGANISME_HPP
#define ORGANISME_HPP

#include <Arbre.hpp>
#include <list>
#include "Celula.hpp"

/**
* @class Organisme
* @brief Representa un organisme
*/

class Organisme
{

public:
    /**
    * @brief Crea un organisme buit
    */
    Organisme();

    /**
    * @brief Fa que el P.I. sigui el fill de p1 i p2
    * @pre el P.I. es buit. p1 i p2. són compatibles
    * @post el P.I. és l'organisme resultant de l'aparellament entre el p1 i p2
    * @param p1 organisme amb el que aparellar p2
    * @param p2 organisme amb el que aparellar p1
    */
    void reproduir(Organisme &p1, Organisme &p2);


    /**
    * @brief Llegeix un organisme desde l'stdin
    * @pre el P.I. és buit
    * @post el P.I. està format per les dades que hi havien a l'stdin
    */
    void llegeix_stdin();

    /**
    * @brief Escriu un organisme a l'stdout
    * @pre cert
    * @post a l'stdout hi han les dades de l'organisme
    */
    void escriu_stdout();

    /**
    * @brief Diu si l'organisme del paràmetre implícit és compatible
    *        amb l'organisme o
    * @param o l'organisme amb el qual comprovar la compatibilitat
    * @pre cert
    * @post retorna si són compatibles
    * @return si són compatibles
    */
    bool es_compatible(Organisme &o);

    /**
    * @brief Fa créixer l'organisme
    * @pre cert
    * @post l'organisme ha crescut
    */
    void creix();

    /**
    * @brief Fa decréixer l'organisme, és a dir, elimina totes les
    *        cèl·lules que no tenen cap filla
    * @pre l'organisme pot decréixer
    * @post l'organisme ha decrescut
    */
    void decreix();

    /**
    * @brief Ens diu el nombre de cèl·lules que té l'organisme
    * @pre cert
    * @post retorna el nombre de cèl·lules que té l'organisme
    * @return el nombre de cèl·lules que té l'organisme
    */
    int numero_celules();

    /**
    * @brief Ens diu si l'organisme està viu, és a dir, si no ha
    *        perdut la seva cèl·lula original
    * @pre cert
    * @post retorna si l'organisme està viu
    * @return si l'organisme està viu
    */
    bool esta_viu() const;

private:
    /**
    * @brief Reprodueix, seguint les indicacions del enunciat de la pràctica,
    *        els organismes a1 i a2 i posa el resultat de la seva reproducció a fill.
    *        A més a més retorna el ID màxim de fill i el seu nombre de cèl·lules.
    * @pre max_ID_o1 conté el ID màxim d'a1
    * @post després de diverses crides recursives, el paràmetre fill conté l'arbre resultant
    *       de l'aparellament entre a1 i a2, max_ID conté el ID màxim, i n_cel el nombre de cèl·lules
    */
    static void i_reproduccio(Arbre<Celula> &a1, Arbre<Celula> &a2, Arbre<Celula> &fill, bool &plantar, int &max_ID_o1, int &max_ID, int &n_cel);
    
    
    /**
    * @brief Calcula el tamany de l'arbre resultant de l'intersecció entre t1 i t2
    * @pre cert
    * @post tamany_fill conté el nombre de cèl·lules de l'intersecció
    */    
    static void interseccio(Arbre<Celula> &t1, Arbre<Celula> &t2, int &tamany_fill);
    
    /**
    * @brief Fa créixer l'arbre t, i ens diu el seu ID màxim i el nombre de cèl·lules
    * @pre t = T; max_ID té l'ID màxim de t, i tamany el seu tamany
    * @post t ha crescut, i max_ID i tamany contenen el ID màxim i el tamany de T, respectivament
    */ 
    static void creix_arbre(Arbre<Celula> &t, int &max_ID, int &tamany);
    
    /**
    * @brief Fa decréixer l'arbre t, i ens diu el seu ID màxim i el nombre de cèl·lules
    * @pre t és un arbre no buit
    * @post max_ID conté l'ID màxim de les cèl·lules un cop ha decrescut, a més a més
    *       tamany conté el nombre de cèl·lules
    */ 
    static void decreix_arbre(Arbre<Celula> &t, int &max, int &tamany);
    
    /**
    * @brief Llegeix per stdin un arbre en preordre, tot seguint l'estructura
    *        descrita a l'enunciat de la pràctica
    * @pre cert
    * @post max_ID conté l'ID màxim de les cèl·lules, a més a més tamany
    *       conté el nombre de cèl·lules
    */ 
    static void llegeix_preordre(Arbre<Celula> &t, int &tamany, int &max_ID);
    
    /**
    * @brief Escriu per stdout un arbre en inordre, tot seguint l'estructura
    *        descrita a l'enunciat de la pràctica
    * @pre cert
    * @post l'arbre t ha estat escrit per stdout
    */ 
    static void escriu_inordre(Arbre<Celula> &t);


    /** @brief ID màxim de totes les cèl·lules de l'organisme */
    int max_ID;
    /** @brief nombre de cèl·luls que té l'organisme */
    int tamany;
    /** @brief indica si l'organisme ha estat retallat */
    bool retallat;
    /** @brief arbre que conté les cèl·lules de l'organisme */
    Arbre<Celula> celules;
};



#endif
